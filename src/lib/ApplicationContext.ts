import { Application } from "../backend/Application";
import { Constructor, Mixin } from "../util/mixin";

export interface ApplicationContext {
  application: Application;
}

export function ApplicationContextMixin<T extends Constructor<any>>(Base: T) {
  @Mixin("ApplicationContext")
  abstract class BaseApplicationContext
    extends Base
    implements ApplicationContext {
    application: Application;

    constructor(...args: any[]) {
      let app = undefined;
      if (args[0] instanceof Application) {
        app = args[0];
        args = args.slice(1);
      } else {
        app = Application.fromContext();
      }
      if (!app) {
        throw new Error("ApplicationContext must be scoped to an application");
      }
      super(...args);
      this.application = app;
      this.initApplicationContext();
    }

    protected initApplicationContext(): void {
      // nothing to do
    }
  }

  return BaseApplicationContext;
}
