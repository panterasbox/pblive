import { Context, Action } from "../backend/Backend";
import { Interactive } from "../backend/Interactive";

export type Settings = Record<string, any>;

export interface InitButton {
  settings: Settings;
}

export interface ResetButton {
}

export class StreamDeckButton {
  interactive: Interactive;
  context: Context;
  function: string;
  settings: Settings;

  constructor(interactive: Interactive, initAction: Action<InitButton>) {
    this.interactive = interactive;
    this.context = initAction.context;
    this.function = initAction.payload.settings.function;
    this.settings = initAction.payload.settings;
  }

  reset(resetAction: Action<ResetButton>) {
    // nothin
  }

}