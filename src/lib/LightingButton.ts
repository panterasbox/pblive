import * as path from "path";
import * as fs from "fs";
import { Action } from "../backend/Backend";
import { Interactive } from "../backend/Interactive";
import { LightingController } from "./LightingController";
import { InitButton, ResetButton, StreamDeckButton } from "./StreamDeckButton";

export class LightingButton extends StreamDeckButton {
  controller: LightingController;
  _switch?: boolean;
  _rgb?: boolean;
  _cycle?: boolean;

  constructor(
    interactive: Interactive, 
    initAction: Action<InitButton>, 
    controller: LightingController) {
    
    super(interactive, initAction);
    this.controller = controller;
    controller.onInitButton(this);
  }

  reset(resetAction: Action<ResetButton>) {
    super.reset(resetAction);
    this.controller.onResetButton(this, resetAction);
  }

  get switch() { return this._switch === true; }
  
  get rgb() { return this._rgb === true; }

  get cycle() { return this._cycle === true; }

  set switch(zwitch: boolean) {
    this._switch = zwitch;
    if (this.function === "lighting.toggleLight") {
      const fname = path.join(__dirname, "..", "..", "images", 
      (this._switch ? `lightsOn.png` : `lightsOff.png`));
      fs.readFile(
        fname, 
        { encoding: 'base64'},
        (err, image) => {
          this.controller.application.sendCommand(this.interactive, {
            event: "setImage",
            context: this.context,
            payload: {
              image: `data:image/png;base64,${image}`
            }
          });
        }
      );
    }
  }

  set rgb(rgb: boolean) {
    this._rgb = rgb;
    if (this.function === "lighting.toggleMode") {
      fs.readFile(
        path.join(__dirname, "..", "..", "images", 
          (this._rgb ? `blueLight.png` : `lightsOn.png`)), 
        { encoding: 'base64'},
        (err, image) => {
          this.controller.application.sendCommand(this.interactive, {
            event: "setImage",
            context: this.context,
            payload: {
              image: `data:image/png;base64,${image}`
            }
          });
        }
      );
    }
  }

  set cycle(cycle: boolean) {
    this._cycle = cycle;
    if (this.function === "lighting.toggleCycle") {
      fs.readFile(
        path.join(__dirname, "..", "..", "images", 
          (this._cycle ? `cycleLight.png` : `lightsOn.png`)), 
        { encoding: 'base64'},
        (err, image) => {
          this.controller.application.sendCommand(this.interactive, {
            event: "setImage",
            context: this.context,
            payload: {
              image: `data:image/png;base64,${image}`
            }
          });
        }
      );
    }
  }

}