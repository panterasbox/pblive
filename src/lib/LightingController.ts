import { ApplicationContextMixin } from "./ApplicationContext";
import { Application } from "../backend/Application";
import { ZigbeeDevice, MakerApi } from "../backend/MakerApi";
import { Action, Context } from "../backend/Backend";
import { LightingButton } from "./LightingButton";
import { ResetButton } from "./StreamDeckButton";

const DEVICE = "Office";
const CYCLE_INTERVAL = 5000;
const REFRESH_INTERVAL = 15000;

export interface ToggleLight {
}

const _LightingController = ApplicationContextMixin(Object);

export class LightingController extends _LightingController {
  makerApi: MakerApi;
  zigbeeDevices: Record<string, ZigbeeDevice> = { };
  cycleTimeout?: NodeJS.Timeout;
  refreshTimeout?: NodeJS.Timeout;
  buttons: Record<Context, LightingButton> = { };

  constructor(application: Application, makerApi: MakerApi) {
    super(application);
    this.makerApi = makerApi;
  }

  async initialize() {
    await this.refreshDevices();
  }

  updateContext(button: LightingButton, context: Context) {
    if (!(context in this.buttons)) {
      this.buttons[context] = button;
    }
    // if (button.context != context) {
    //   if (button.context in this.buttons) {
    //     delete this.buttons[button.context];
    //   }
    //   button.context = context;
    // }
  }

  onInitButton(button: LightingButton) {
    this.updateContext(button, button.context);
  }

  onResetButton(button: LightingButton, action: Action<ResetButton>) {
    this.updateContext(button, action.context);
  }

  async getDevice(): Promise<ZigbeeDevice | undefined> {
    let device = this.zigbeeDevices[DEVICE];
    if (!device) {
      await this.refreshDevices();
      device = this.zigbeeDevices[DEVICE];
    }
    return device;
  }

  async toggleLight(action: Action<ToggleLight>, button: LightingButton) {
    this.updateContext(button, action.context);

    const device = await this.getDevice();
    if (!device) {
      this.application.sendAlert(button);
      return;
    }

    const zwitch = device.attributes.switch === "on" ? "off" : "on";
    if (zwitch === "off" && this.cycleTimeout) {
      clearTimeout(this.cycleTimeout);
      this.cycleTimeout = undefined;
      button.cycle = false;
    }
    await this.makerApi.setSwitch(device.id, zwitch);

    this.application.sendOk(button);

    this.refreshDevices();
  }

  async toggleMode(action: Action<ToggleLight>, button: LightingButton) {
    this.updateContext(button, action.context);

    const device = await this.getDevice();
    if (!device) {
      this.application.sendAlert(button);
      return;
    }

    if (device.attributes.colorMode === "RGB") {
      if (this.cycleTimeout) {
        clearTimeout(this.cycleTimeout);
        this.cycleTimeout = undefined;
        button.cycle = false;
      }
      await this.makerApi.setColorTemperature(device.id, 4000);
      await this.makerApi.setLevel(device.id, 100);
    } else {
      await this.makerApi.setHue(device.id, 60);
      await this.makerApi.setSaturation(device.id, 100);
      await this.makerApi.setLevel(device.id, 100);
    }

    this.application.sendOk(button);

    this.refreshDevices();
  }

  async toggleCycle(
    action: Action<ToggleLight>, button: LightingButton) {

    this.updateContext(button, action.context);
    const device = await this.getDevice();
    if (!device) {
      this.application.sendAlert(button);
      return;
    }

    if (this.cycleTimeout) {
      clearTimeout(this.cycleTimeout);
      this.cycleTimeout = undefined;
      button.cycle = false;
    } else {
      await this.makerApi.setSaturation(device.id, 100);
      await this.makerApi.setLevel(device.id, 100);
  
      const cycleUp = async () => {
        const device = await this.getDevice();
        if (!device) {
          this.application.sendAlert(button);
          return;
        }

        const hue = parseInt(device.attributes.hue) % 100;

        await this.makerApi.setHue(device.id, hue + 1);
        this.refreshDevices();
      }
      this.cycleTimeout = setInterval(cycleUp, CYCLE_INTERVAL)
      button.cycle = true;
    }

  }

  async refreshDevices() {
    if (this.refreshTimeout) {
      clearTimeout(this.refreshTimeout);
      this.refreshTimeout = undefined;
    }

    const devices = await this.makerApi.getDevices();
    this.broadcastChanges(devices, this.zigbeeDevices);
    this.zigbeeDevices = devices;

    this.refreshTimeout = 
      setTimeout(this.refreshDevices.bind(this), REFRESH_INTERVAL);
  }

  broadcastChanges(
    newDevices: Record<string, ZigbeeDevice>,
    oldDevices: Record<string, ZigbeeDevice>
  ) {
    const device = oldDevices[DEVICE];
    if (device) {
      const oldAttr = device.attributes;
      const newAttr: Record<string, any> = DEVICE in newDevices
        ? newDevices[DEVICE].attributes
        : {};
      if (newAttr.switch != oldAttr.switch) {
        Object.values(this.buttons).forEach(button => 
          button.switch = newAttr.switch === "on");
      }
      if (newAttr.colorMode != oldAttr.colorMode) {
        Object.values(this.buttons).forEach(button => 
          button.rgb = newAttr.colorMode === "RGB");
      }
    }
  }

}
