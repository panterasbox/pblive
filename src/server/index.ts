import WebSocket from "ws";
import AWS from "aws-sdk";
import express from "express";
import session from "express-session";
import * as http from "http";
import { promisify } from "util";
import sessionStore from "./sessionStore";
import config from "../util/config";
import { Backend } from "../backend/Backend";

AWS.config.update({ region: config.region });

const SESSION_COOKIE = "panterasbot.sid";

export const spawnServer = async (backend: Backend) => {
  const SESSION_SECRET = backend.getSecret("session_secret");

  const app = express();

  app.use(express.json());

  const sessionParser = session({
    name: SESSION_COOKIE,
    store: sessionStore,
    secret: SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
  });

  app.use(sessionParser);

  const server = http.createServer(app);

  app.get("*", (req, res) => {
    res.send(400);
  });

  const wss = new WebSocket.Server({ server, path: "/" });
  backend.connectWsServer(wss);

  wss.on("connection", (ws, req) => {
    backend.onClientConnection(ws);
    ws.on("message", (msg: string) => {
      backend.onClientMessage(ws, msg);
    });
    ws.on("close", (code: number, reason: string) => {
      backend.onClientClose(ws, code, reason);
    });
  });

  server.listen(backend.getParameter("server_port"), () => {
    console.info(`Server started on ${server.address()} :)`);
  });

  return server;
};

const getSession = async (
  req: http.IncomingMessage
): Promise<session.SessionData> => {
  const cookies = req.headers.cookie?.split(/;\s*/) || [];
  for (let i = 0, j = cookies?.length || 0; i < j; i++) {
    const parts = cookies[i].split("=", 2);
    if (parts && parts[0] === SESSION_COOKIE) {
      const sessionKey = decodeURIComponent(parts[1]);
      const matches = sessionKey.match(/^s:(.*?)\..*/);
      if (matches) {
        const sid = matches[1];
        const getFromStore = promisify(sessionStore.get.bind(sessionStore));
        const setInStore = promisify(sessionStore.set.bind(sessionStore));
        let sessionData = await getFromStore(sid);
        if (!sessionData) {
          sessionData = {
            sid: sid,
            cookie: new session.Cookie(),
          };
        } else {
          sessionData.sid = sid;
        }
        await setInStore(sid, sessionData);
        sessionData = await getFromStore(sid);
        if (!sessionData) {
          throw new Error("error initializing session data");
        }
        return sessionData;
      }
    }
  }
  throw new Error("session cookie not found in headers");
};
