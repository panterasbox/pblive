import WebSocket from "ws";
import { StreamDeckButton } from "../lib/StreamDeckButton";
import { Context } from "./Backend";

export class Interactive {
  websocket: WebSocket;
  buttons: Record<Context, StreamDeckButton> = { };

  constructor(ws: WebSocket) {
    this.websocket = ws;
  }

}
