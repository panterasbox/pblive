import WebSocket from "ws";
import { getSecrets, getParameters } from "../util/aws";
import { Interactive } from "./Interactive";
import { Application } from "./Application";
import { MakerApi } from "./MakerApi";

export type Context = string;

export interface Action<T> {
  action: string,
  context: Context,
  payload: T
}

export interface Command<T> {
  event: string,
  context: Context,
  payload: T
}

export class Backend {
  websocketServer: WebSocket.Server | undefined;
  websockets: Map<WebSocket, Interactive>;
  secrets: { [key: string]: any } | undefined;
  parameters: { [key: string]: any } | undefined;
  application: Application;
  makerApi: MakerApi;

  constructor() {
    this.websockets = new Map();
    this.makerApi = new MakerApi(this);
    this.application = new Application(this);
  }

  async initialize() {
    this.secrets = await getSecrets();
    this.parameters = await getParameters();

    await this.makerApi.initialize();
    await this.application.initialize();
  }

  getSecret(secret: string): any {
    return this.secrets?.[secret];
  }

  getParameter(param: string): string {
    return this.parameters?.[param];
  }

  connectWsServer(wss: WebSocket.Server) {
    if (this.websocketServer && this.websocketServer.clients.size > 0) {
      throw new Error(
        "Can't reconnect websocket server while backend has active clients"
      );
    }
    this.websocketServer = wss;
    this.websockets.clear();
  }

  async onClientConnection(ws: WebSocket) {
    console.debug("Connected");
    const interactive = new Interactive(ws);

    this.websockets.set(ws, interactive);

    this.application.onConnect(interactive);
  }

  onClientMessage(ws: WebSocket, msg: any) {
    console.debug("received: %s", msg);
    let action = JSON.parse(msg);
    const interactive = this.websockets.get(ws);
    if (interactive) {
      this.application.onAction(interactive, action);
    }
  }

  onClientClose(ws: WebSocket, code: number, reason: string) {
    console.log("Websocket closed", code, reason);
    this.websockets.delete(ws);
  }

  sendClientMessage(ws: WebSocket, command: Command<any>) {
    let msg = JSON.stringify(command);
    console.debug("sending: %s", msg);
    ws.send(msg, (err) => console.error(err));
  }
  
}
