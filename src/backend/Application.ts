import callsites from "callsites";
import { ApplicationContext } from "../lib/ApplicationContext";
import { LightingButton } from "../lib/LightingButton";
import { LightingController } from "../lib/LightingController";
import { StreamDeckButton } from "../lib/StreamDeckButton";
import { hasMixin } from "../util/mixin";
import { Backend, Command, Action } from "./Backend";
import { Interactive } from "./Interactive";

export class Application {
  private backend: Backend;
  controllers: Record<string, any>;

  constructor(backend: Backend) {
    this.backend = backend;
    this.controllers = {
      "lighting" : new LightingController(this, backend.makerApi)
    }
  }

  static fromContext(): Application | undefined {
    const stack = callsites().splice(1);
    for (let callsite of stack) {
      const ob = callsite.getThis();
      if (!ob) {
        continue;
      }

      if (hasMixin(ob, "ApplicationContext")) {
        return (ob as ApplicationContext).application;
      }

      if (ob instanceof Application) {
        return ob as Application;
      }
    }
    return undefined;
  }

  async initialize() {
  }

  async start() {
    await this.controllers["lighting"].start(this.backend.makerApi);
  }

  onConnect(interactive: Interactive) {
  }

  onAction(interactive: Interactive, action: Action<any>) {
    const initButton = () => {
      if (!(action.context in interactive.buttons)) {
        const [ controller, func ] = 
          action.payload.settings.function.split(".", 2);
        switch (controller) {
          case "lighting":
            interactive.buttons[action.context] = new LightingButton(
              interactive, action, this.controllers["lighting"]);
            break;
          default:
            interactive.buttons[action.context] = new StreamDeckButton(
              interactive, action);
            break;
        }
      }
    }
    switch (action.action) {
      case "init":
        initButton();
        break;
      case "reset": 
        initButton();
        interactive.buttons[action.context].reset(action);
        break;
      case "execute":
        initButton();
        const [ controller, func ] = 
          action.payload.settings.function.split(".", 2);
        const f = this.controllers[controller] 
          ? this.controllers[controller][func]
                .bind(this.controllers[controller])
          : undefined;
        if (!f) {
          this.sendAlert(interactive.buttons[action.context]);
        } else {
          f(action, interactive.buttons[action.context]);
        }
        break;
    }
  }

  sendCommand(interactive: Interactive, command: Command<any>) {
    this.backend.sendClientMessage(interactive.websocket, command);
  }
  
  sendOk(button: StreamDeckButton) {
    this.sendCommand(button.interactive, {
      "event": "showOk",
      "context": button.context,
      "payload": {}
    });
  }

  sendAlert(button: StreamDeckButton) {
    this.sendCommand(button.interactive, {
      "event": "showAlert",
      "context": button.context,
      "payload": {}
    });
  }

  disconnect(interactive: Interactive) {
  }


}
