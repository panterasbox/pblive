import axios, { AxiosResponse } from "axios";
import { Backend } from "./Backend";

export interface Command {
  command: string;
}

export interface Attributes {
  RGB?: string;
  color?: string;
  colorMode: string;
  colorName: string;
  colorTemperature: string;
  hue: string;
  level: string;
  saturation: string;
  switch: "off" | "on";
}

export interface ZigbeeDevice {
  id: string;
  name: string;
  label: string;
  type: string;
  attributes: Attributes;
  commands: Command[];
}

export class MakerApi {
  backend: Backend;
  makeApiUri!: string;
  accessToken!: string;

  constructor(backend: Backend) {
    this.backend = backend;
  }

  initialize() {
    this.accessToken = this.backend.getSecret("make_api_access_token");
    this.makeApiUri = this.backend.getParameter("make_api_uri");
  }

  async callMakeApi(path: string, params = {}) {
    console.log('makeapi', `${this.makeApiUri}${path}`);
    return await axios.get(`${this.makeApiUri}${path}`, {
      params: {
        ...params,
        "access_token": this.accessToken
      }
    });
  }

  async getDevices(): Promise<Record<string, ZigbeeDevice>> {
    const response: AxiosResponse<ZigbeeDevice[]> = 
      await this.callMakeApi('/devices/all');
    return response.data.reduce((prev, cur) => {
      prev[cur.name] = cur;
      return prev;
    }, {} as Record<string, ZigbeeDevice>);
  }

  async getDevice(name: string): Promise<ZigbeeDevice | undefined> {
    const response: AxiosResponse<ZigbeeDevice[]> = 
      await this.callMakeApi('/devices/all');
    return response.data.reduce((prev: ZigbeeDevice | undefined, cur) => {
      return cur.name == name ? cur : prev;
    }, undefined);
  }

  async setSwitch(device: string, zwitch: "on" | "off") {
    await this.callMakeApi(`/devices/${device}/${zwitch}`);
  }

  async setLevel(device: string, level: number) {
    await this.callMakeApi(`/devices/${device}/setLevel/${level}`);
  }

  async setColorTemperature(device: string, temp: number) {
    await this.callMakeApi(`/devices/${device}/setColorTemperature/${temp}`);
  }

  async setHue(device: string, hue: number) {
    await this.callMakeApi(`/devices/${device}/setHue/${hue}`);
  }

  async setSaturation(device: string, level: number) {
    await this.callMakeApi(`/devices/${device}/setSaturation/${level}`);
  }

}