require("source-map-support").install();
import "reflect-metadata";
import { spawnServer } from "./server/";
import { Backend } from "./backend/Backend";

const start = async () => {
  const backend = new Backend();

  console.debug("Initializing backend");
  await backend.initialize();

  console.debug("Spawning server");
  const server = await spawnServer(backend);
};

start().then(() => {
  console.debug("Started.");
});
