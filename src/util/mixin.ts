export type Constructor<T = {}> = new (...args: any[]) => T;

const mixins = new Map<string, Set<Function>>();

export function Mixin(name: string) {
  if (!mixins.has(name)) {
    mixins.set(name, new Set());
  }
  return function <T extends Constructor>(constructor: T) {
    mixins.get(name)?.add(constructor);
    return constructor;
  };
}

export function isMixin(constructor: Function, mixin: string): boolean {
  return mixins.get(mixin)?.has(constructor) || false;
}

export function hasMixin(instance: any, mixin: string) {
  const constructor = instance.constructor;

  const visited = new Set<Function>();
  let frontier = new Set<Function>();
  frontier.add(constructor);

  while (frontier.size > 0) {
    const intersection = new Set(
      [...frontier].filter((x) => mixins.get(mixin)?.has(x))
    );
    if (intersection.size) {
      return true;
    }
    frontier.forEach((item) => visited.add(item));

    const newFrontier = new Set<Function>();
    frontier.forEach((item) => {
      const itemConstituents = protoChain(item.prototype)
        .map((proto) => proto.constructor)
        .filter((item) => item !== null);
      if (itemConstituents)
        itemConstituents.forEach((constituent) => {
          if (!visited.has(constituent) && !frontier.has(constituent))
            newFrontier.add(constituent);
        });
    });

    // we have a new frontier, now search again
    frontier = newFrontier;
  }

  // if we get here, we couldn't find the mixin anywhere in the prototype chain or associated mixin classes
  return false;
}

const protoChain = (obj: object, currentChain: object[] = [obj]): object[] => {
  const proto = Object.getPrototypeOf(obj);
  if (proto === null) return currentChain;

  return protoChain(proto, [...currentChain, proto]);
};
