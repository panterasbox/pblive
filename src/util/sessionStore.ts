import * as session from "express-session";
import { promisify } from "util";

declare module "express-session" {
  interface SessionData {
    sid: string;
    twitchId?: string;
    gitlabId?: number;
    passport?: { user: any };
    twitchReferer?: string;
    gitlabReferer?: string;
  }
}
let sessionStore: session.Store;

sessionStore = new session.MemoryStore();

export const saveSession = async (session: session.SessionData) => {
  if (session) {
    const sid = session.sid;
    if (sid) {
      const setInStore = promisify(sessionStore.set.bind(sessionStore));
      return await setInStore(sid, session);
    }
  }
};

export default sessionStore;
